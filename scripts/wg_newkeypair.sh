#!/usr/bin/env bash

# Generate a new private/public keypair for Wireguard that can be used in the
# Ansible configs

# Create private key
privkey=$(wg genkey)

# Create public key derived from private key
pubkey=$(echo "$privkey" | wg pubkey)

# ansible-vault enctypt private key
privkey_enc=$(echo -n "$privkey" | ansible-vault encrypt_string --stdin-name "wg_private_key" 2> /dev/null)

# Output keys
echo "wg_public_key: $pubkey"
echo "$privkey_enc"
