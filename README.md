# Proxmox VM Backup

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/proxmox-backup/00_README)

This playbook and its roles configure the server and clients for backups of some
Proxmox VMs.

The server is a VM with lots of available space, providing a NFS storage. The
clients are Proxmox nodes. To be able to backup VMs from one cluster, *all*
nodes of this cluster must be clients. The clients are connected to the server
via a Wireguard tunnel, but not among each other.

## New client

In order to add a new Proxmox node, execute these steps:

* Run `./scripts/wg_newkeypair.sh`. This will generate a new private/public
  keypair for this host.
* Create a new `host_vars` file for this host. You will need to set a unique
  internal Wireguard address within the subnet `10.5.31.1/24`. Also add the
  generated keys in there.
* Re-run the playbook. You can limit it to the server and the newly added host.
  The other clients will not connect to this host.

You will need to be able to decrypt the `vaultpw.gpg` file in order to gain the
password for the Ansible vault encryption.

## Establish new NFS connection in Proxmox

In the Proxmox UI of the cluster you would like to connect to the NFS storage,
do the following:

* Add a new directory under the `nfs_dirs` vars of the NFS server, and run the
  playbook. The name should be able to identify the cluster.
* Go to Datacenter > Storage
* Add a new storage with the type NFS. Give it an ID, and provide the following info:
  * Server: `10.5.31.1` (value of the server's `wg_address` vars)
  * Export: the location on the NFS server you would like to write to. Select
    the `nfs_dirs` directory you just created.
  * Content: only select `VZDump backup file`
  * Select the Backup Retention as wished. Check the other clusters to see how
    it's done.

Leave the rest as default and press OK.

Now, you can either set up backup jobs with this storage, or backup individual
VMs with this storage as destination. Note that in order to see the backups on
this storage, you'll have to select it on the top right in the VM's backup tab.
